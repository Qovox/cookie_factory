package fr.unice.polytech.cookiefactory15;

import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class Shop {

    private String shopName;
    private String address;
    private State state;
    private Collection<Manager> managers;
    private LocalTime date_open;
    private LocalTime date_close;
    private Stock stock;
    private List<Order> orders;
    private Stats stats;

    public Shop(String shopName) {
        this.shopName = shopName;
        address = "No address currently available";
        state = State.ALABAMA;
        managers = new ArrayList<>();
        date_open = LocalTime.of(8, 0);
        date_close = LocalTime.of(18, 0);
        stock = new Stock();
        orders = new ArrayList<>();
        stats = new Stats();
    }

    //Completed constructor
    public Shop(String shopName, String address, State state,
                LocalTime date_open, LocalTime date_close,
                Collection<Manager> managers) {
        this.shopName = shopName;
        this.address = address;
        this.state = state;
        this.date_open = date_open;
        this.date_close = date_close;
        this.managers = managers;
        stock = new Stock();
        orders = new ArrayList<>();
        stats = new Stats();
    }

    public void order(Customer customer, LocalDateTime date, Map<Orderable, Integer> cookie_to_order, PaymentMode paymentMode) throws Exception {
        Order order = new Order(customer, this, date, cookie_to_order, paymentMode);
        orders.add(order);
        for (Orderable cookie : cookie_to_order.keySet()) {
            stock.removeIngredientsFromCookie(cookie, cookie_to_order.get(cookie));
        }
        stats.newOrder(date);
    }

    public void addManager(Manager manager) {
        managers.add(manager);
    }

    public String getShopName() {
        return shopName;
    }

    public Optional<Manager> getManagerByName(String arg0) {
        for (Manager manager : managers) {
            if (manager.getName().equals(arg0)) return Optional.of(manager);
        }
        return Optional.empty();
    }

    public void changeHour(Manager manager, String newOpenHour, boolean openHour) {
        String[] time = newOpenHour.split(":");
        if (time.length < 2) return; //we should have an hour and minutes
        int hours = Integer.parseInt(time[0]);
        int minutes = Integer.parseInt(time[1]);

        if (openHour) date_open = LocalTime.of(hours, minutes);
        else date_close = LocalTime.of(hours, minutes);
    }

    public String getHourToString(boolean openHour) {
        if (openHour) return date_open.getHour() + ":" + date_open.getMinute();
        else return date_close.getHour() + ":" + date_close.getMinute();
    }

    public boolean orderIsDoable(Order order) {
        for (Orderable orderable :
                order.getCookie_to_order().keySet()) {
            if (!stock.isDoable(orderable, order.getCookie_to_order().get(orderable))) {
                return false;
            }
        }

        return true;
    }

    public State getState() {
        return state;
    }

    public Stock getStock() {
        return stock;
    }

    public List<Order> getOrders() {
        return orders;
    }

    @Override
    public String toString() {
        String shop = "Nombre de manager : ";
        shop += Integer.toString(managers.size());
        return shop;
    }

    public void printStats() {
        System.out.println(stats.print());
    }

    public Stats getStats() {
        return stats;
    }

    public String openingHour() {
        return date_open.toString();
    }

    public String closingHour() {
        return date_close.toString();
    }

    public LocalTime getDateClose() {
        return date_close;
    }

}