package fr.unice.polytech.cookiefactory15;

import fr.unice.polytech.cookiefactory15.cookieparts.Dough;
import fr.unice.polytech.cookiefactory15.cookieparts.Flavour;
import fr.unice.polytech.cookiefactory15.cookieparts.Topping;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.util.EnumMap;
import java.util.Map;

public class Stock {

    private Map<Dough, Integer> dough;
    private Map<Flavour, Integer> flavour;
    private Map<Topping, Integer> topping;

    public Stock() {
        dough = new EnumMap<>(Dough.class);
        flavour = new EnumMap<>(Flavour.class);
        topping = new EnumMap<>(Topping.class);
    }

    public void addToDough(Dough dough, int amount) {
        if (this.dough.containsKey(dough))
            this.dough.put(dough, this.dough.get(dough) + amount);
        else this.dough.put(dough, amount);
    }

    public void addToFlavour(Flavour flavour, int amount) {
        if (this.flavour.containsKey(flavour))
            this.flavour.put(flavour, this.flavour.get(flavour) + amount);
        else this.flavour.put(flavour, amount);
    }

    public void addToTopping(Topping topping, int amount) {
        if (this.topping.containsKey(topping))
            this.topping.put(topping, this.topping.get(topping) + amount);
        else this.topping.put(topping, amount);
    }

    public void removeIngredientsFromCookie(Orderable orderable, int amount) {
        Map<Dough, Integer> doughs = orderable.getDoughToMap();
        Map<Flavour, Integer> flavours = orderable.getFlavourToMap();
        Map<Topping, Integer> toppings = orderable.getToppingToMap();

        for (Dough dough : doughs.keySet()) {
            this.dough.put(dough, this.dough.get(dough) - amount * doughs.get(dough));
        }

        for (Flavour flavour : flavours.keySet()) {
            this.flavour.put(flavour, this.flavour.get(flavour) - amount * flavours.get(flavour));
        }

        for (Topping topping : toppings.keySet()) {
            this.topping.put(topping, this.topping.get(topping) - amount * toppings.get(topping));
        }
    }

    public int getDoughIngredientAmount(Dough dough) {
        return this.dough.get(dough);
    }

    public int getFlavourIngredientAmount(Flavour flavour) {
        return this.flavour.get(flavour);
    }

    public int getToppingIngredientAmount(Topping topping) {
        return this.topping.get(topping);
    }

    public boolean isDoable(Orderable orderable) {
        return isDoable(orderable, 1);
    }

    public boolean isDoable(Orderable orderable, int number) {
        Map<Dough, Integer> doughs = orderable.getDoughToMap();
        Map<Flavour, Integer> flavours = orderable.getFlavourToMap();
        Map<Topping, Integer> toppings = orderable.getToppingToMap();

        for (Dough dough : doughs.keySet()) {
            if (this.dough.get(dough) < number * doughs.get(dough)) {
                return false;
            }
        }

        for (Topping topping : toppings.keySet()) {
            if (this.topping.get(topping) < number * toppings.get(topping)) {
                return false;
            }
        }

        for (Flavour flavour : flavours.keySet()) {
            if (this.flavour.get(flavour) < number * flavours.get(flavour)) {
                return false;
            }
        }

        return true;
    }

}