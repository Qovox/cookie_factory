package fr.unice.polytech.cookiefactory15.customers;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;

public class Customer {

    private int ID;
    private String name;
    private boolean hasAccount;
    private CustomerAccount account;

    public Customer(String name) {
        this.name = name;
        ID = TheCookieFactory.nextID++;
        hasAccount = false;
    }

    public void createAccount() {
        hasAccount = true;
        account = new CustomerAccount();
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public boolean hasAccount() {
        return hasAccount;
    }

    public CustomerAccount getCustomerAccount() {
        return account;
    }

}
