package fr.unice.polytech.cookiefactory15.customers;

import fr.unice.polytech.cookiefactory15.Order;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CustomerAccount {

    private boolean isPremium;
    private Collection<Order> orders;
    private LocalDate enrollment;
    private int cookieOrdered;
    private Map<LocalDateTime, Double> vouchers;

    public CustomerAccount() {
        this.isPremium = false;
        this.cookieOrdered = 0;
        orders = new ArrayList<>();
        enrollment = LocalDate.now();
        vouchers = new HashMap<>();
    }

    public boolean isEligible(int nbCookieOrdered) {
        if (cookieOrdered >= TheCookieFactory.COOKIE_DISCOUNT) {
            this.cookieOrdered = nbCookieOrdered;
            return true;
        } else {
            this.cookieOrdered += nbCookieOrdered;
        }

        return false;
    }

    public void becomePremium() {
        this.isPremium = true;
    }

    public void cancelPremium() {
        this.isPremium = false;
    }

    public void addVoucher(LocalDateTime date, Double amount) {
        vouchers.put(date, amount);
    }

    public void checkVoucher() {
        LocalDateTime now = LocalDateTime.now();
        vouchers.entrySet().removeIf(entry -> entry.getKey().plusMonths(TheCookieFactory.VOUCHER_MONTH_LIFESPAN).isAfter(now));
    }

    public double lastVoucher() {
        LocalDateTime olderVoucher = LocalDateTime.now();
        for (LocalDateTime voucher : vouchers.keySet()) {
            if (voucher.isBefore(olderVoucher)) {
                olderVoucher = voucher;
            }
        }
        if (vouchers.containsKey(olderVoucher))
            return vouchers.remove(olderVoucher);
        return 0;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public int getCookieOrdered() {
        return cookieOrdered;
    }

    public int getSeniority() {
        return LocalDate.now().getYear() - enrollment.getYear();
    }

    public Map<LocalDateTime, Double> getVouchers() {
        return vouchers;
    }

}
