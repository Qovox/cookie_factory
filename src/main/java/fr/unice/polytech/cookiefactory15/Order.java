package fr.unice.polytech.cookiefactory15;

import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;
import fr.unice.polytech.cookiefactory15.pricestrategy.CalcultatePriceStrategy;
import fr.unice.polytech.cookiefactory15.pricestrategy.MemberPriceStrategy;
import fr.unice.polytech.cookiefactory15.pricestrategy.NonMemberPriceStrategy;
import fr.unice.polytech.cookiefactory15.stateOrder.OrderNotPayedSaved;
import fr.unice.polytech.cookiefactory15.stateOrder.StateOrder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

public class Order {

    private LocalDateTime date;
    private Customer customer;
    private Shop place;
    private double price;
    private double dtPrice;
    private Map<Orderable, Integer> cookie_to_order;
    private PaymentMode paymentMode;
    private CalcultatePriceStrategy calculator;
    private StateOrder stateOrder;

    public Order(Customer customer, Shop place, LocalDateTime date, Map<Orderable, Integer> cookie_to_order, PaymentMode paymentMode) throws Exception {
        this.customer = customer;
        this.place = place;
        this.date = date;
        this.cookie_to_order = cookie_to_order;
        this.paymentMode = paymentMode;
        this.stateOrder = new OrderNotPayedSaved();
        if (!(customer.hasAccount() && customer.getCustomerAccount().isPremium())) {
            this.stateOrder = stateOrder.checkHour(cookie_to_order, place.getStock());
        }
        calculatePrice();
    }

    private void calculatePrice() {
        if (TheCookieFactory.get_instance().hasAccount(customer.getID())) {
            calculator = new MemberPriceStrategy(customer, place.getState().getTax(), cookie_to_order, -paymentMode.getPriceShift(), place.getDateClose());
        } else {
            calculator = new NonMemberPriceStrategy(place.getState().getTax(), cookie_to_order, -paymentMode.getPriceShift(), place.getDateClose());
        }
        dtPrice = calculator.calculateDTPrice();
        price = calculator.calculatePrice();
    }

    public double getPrice() {
        return price;
    }

    public double getDutyFreePrice() {
        return dtPrice;
    }

    public Map<Orderable, Integer> getCookie_to_order() {
        return cookie_to_order;
    }

    public String orderSheet() {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String orderSheet = "--- The Cookie Factory ---\nOrder Sheet:\n" + place.getShopName() + " shop.\nYour favorite " +
                "shop awaits you from Monday to Friday at " + place.openingHour() + " until " + place.closingHour() +
                "\n\nOrder details:\n";

        StringBuilder details = new StringBuilder();
        for (Map.Entry cookie : cookie_to_order.entrySet()) {
            details.append(((Cookie) cookie.getKey()).getName()).append(" x").append(cookie_to_order.get(cookie.getKey()))
                    .append("\t\t")
                    .append(formatter.format(cookie_to_order.get(cookie.getKey()) * ((Cookie) cookie.getKey()).calculateTotalPrice()))
                    .append("$\n");
        }

        orderSheet += details + "Total duty free:\t\t" + formatter.format(dtPrice) + "$\n" +
                "Total:\t\t\t\t\t" + formatter.format(price) + "$\n\nOrdered the " + LocalDate.now().toString() + " at " +
                LocalTime.now().toString() + "\nfor the " + date.toLocalDate().toString() + " at " + date.toLocalTime().toString() +
                "\nPayment Mode: " + paymentMode.toString() + "\n\nCustomer details: " + customer.getName();
        if (TheCookieFactory.get_instance().hasAccount(customer.getID())) {
            orderSheet += "\nSubscribed to The Loyalty Program: " +
                    (TheCookieFactory.COOKIE_DISCOUNT - customer.getCustomerAccount().getCookieOrdered()) +
                    " cookie(s) to order until next discount\n";
        }

        return orderSheet;
    }

    public void pay() throws Exception {
        this.stateOrder = stateOrder.pay(cookie_to_order, place.getStock());
    }

    public void validate() throws Exception {
        this.stateOrder = stateOrder.validate();
    }

    public void checkHour() throws Exception {
        this.stateOrder = stateOrder.checkHour(cookie_to_order, place.getStock());
    }

    public void deleteCookies(Map<Orderable, Integer> cookiesToDelete) throws Exception {
        this.stateOrder = stateOrder.deleteCookies(cookie_to_order, cookiesToDelete, date, customer, place.getStock());
    }

    public void cancelOrder() throws Exception {
        this.stateOrder = stateOrder.cancelOrder(cookie_to_order, date, customer, place.getStock());
    }

}