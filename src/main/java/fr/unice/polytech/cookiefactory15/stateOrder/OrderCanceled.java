package fr.unice.polytech.cookiefactory15.stateOrder;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDateTime;
import java.util.Map;

public class OrderCanceled implements StateOrder {

    @Override
    public StateOrder pay(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder validate() throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder checkHour(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder deleteCookies(Map<Orderable, Integer> cookieOrdered, Map<Orderable, Integer> cookiesToDelete, LocalDateTime date, Customer client, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder cancelOrder(Map<Orderable, Integer> cookieOrdered, LocalDateTime date, Customer client, Stock stock) throws Exception {
        throw new Exception();
    }

}
