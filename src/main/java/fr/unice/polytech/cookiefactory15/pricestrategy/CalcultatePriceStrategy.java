package fr.unice.polytech.cookiefactory15.pricestrategy;

public interface CalcultatePriceStrategy {

    double calculatePrice();

    double calculateDTPrice();

}
