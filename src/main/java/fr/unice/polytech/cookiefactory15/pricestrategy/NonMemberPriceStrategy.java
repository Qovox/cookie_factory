package fr.unice.polytech.cookiefactory15.pricestrategy;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalTime;
import java.util.Map;

public class NonMemberPriceStrategy extends BasicPriceStrategy implements CalcultatePriceStrategy {

    public NonMemberPriceStrategy(double stateTax, Map<Orderable, Integer> cookies_to_order,
                                  double paymentModeModifier, LocalTime closingTime) {
        super(stateTax, cookies_to_order, paymentModeModifier, closingTime);
    }

    @Override
    public double calculatePrice() {
        double priceDT = calculateDTPrice();
        double priceWithTax;
        double finalPrice;
        double discount = super.calculateBasicDiscount();
        if (super.getNbCookies() >= 25) {
            discount += ((double) super.getNbCookies() / (double) TheCookieFactory.NB_NEEDED_PROGRESSIVE) / 100;
        }
        priceWithTax = priceDT + calculateBasicTaxes(priceDT);
        finalPrice = priceWithTax - (priceDT * discount);
        return finalPrice;
    }

    @Override
    public double calculateDTPrice() {
        return super.calculateBasicDTPrice();
    }

}
