package fr.unice.polytech.cookiefactory15;

import java.time.LocalDateTime;

public class Stats {

    /**
     * This array represent the different time slots where a customer
     * can pickup his order. As we consider a shop can be opened the whole day
     * and we want to split the time slots every 15 minutes, the we have 24*4 = 96.
     */
    private int[] pickupTime = new int[96];

    public Stats() {

    }

    public void newOrder(LocalDateTime pickup) {
        int index = pickup.getHour() * 4 + pickup.getMinute() / 15;
        pickupTime[index]++;
    }

    public String print() {
        StringBuilder stats = new StringBuilder();
        for (int i = 0; i < pickupTime.length; i++) {
            if (pickupTime[i] != 0) {
                stats.append("Orders pickup at ");
                int hours = i / 4;
                int minutes = (i % 4) * 15;
                stats.append(hours).append(":").append(minutes)
                        .append(" : ").append(pickupTime[i]).append("\n");
            }
        }
        return stats.toString();
    }

    public int getOrderByHour(String pickupTime) {
        String[] time = pickupTime.split(":");
        int hours = 0;
        int minutes = 0;
        if (time.length >= 2) {
            hours = Integer.parseInt(time[0]);
            minutes = Integer.parseInt(time[1]);
        }
        return this.pickupTime[hours * 4 + minutes / 15];
    }

}
