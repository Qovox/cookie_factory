package fr.unice.polytech.cookiefactory15.cookieparts;

public enum Flavour {

    VANILLA(0.2, 0.1),
    CINNAMON(0.3, 0.05),
    CHILI(0.4, 0.15);

    private double price;
    private double margin;

    Flavour(double price, double margin) {
        this.price = price;
        this.margin = margin;
    }

    public double getMargin() {
        return margin;
    }

    public double getPrice() {
        return price * (1 + margin);
    }

    public void setMargin(double margin) {
        this.margin = margin;
    }

    public void changePrice(double newPrice) {
        this.price = newPrice;
    }

}