package fr.unice.polytech.cookiefactory15.cookieparts;

public enum Cooking {

    CRUNCHY(0.4),
    CHEWY(0.6);

    private double price;

    Cooking(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

}