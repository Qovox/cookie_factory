package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StockTest {

    private Stock stocks;

    private Cookie cookieSample;

    @Before
    public void init() {
        this.stocks = new Stock();

        this.stocks.addToDough(Dough.CHOCO, 4);
        this.stocks.addToFlavour(Flavour.VANILLA, 5);
        this.stocks.addToTopping(Topping.MILK_CHOCO, 5);

        this.cookieSample = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withTopping(Topping.MILK_CHOCO)
                .withMix(Mix.MIXED)
                .withName("Sooo Choco")
                .build();
    }

    @Test
    public void removeIngredientsFromCookie() throws Exception {
        this.stocks.removeIngredientsFromCookie(cookieSample, 1);

        assertEquals(this.stocks.getDoughIngredientAmount(Dough.CHOCO), 3);
        assertEquals(this.stocks.getFlavourIngredientAmount(Flavour.VANILLA), 4);
        assertEquals(this.stocks.getToppingIngredientAmount(Topping.MILK_CHOCO), 4);

        this.stocks.removeIngredientsFromCookie(cookieSample, 3);

        assertEquals(this.stocks.getDoughIngredientAmount(Dough.CHOCO), 0);
        assertEquals(this.stocks.getFlavourIngredientAmount(Flavour.VANILLA), 1);
        assertEquals(this.stocks.getToppingIngredientAmount(Topping.MILK_CHOCO), 1);
    }

    @Test
    public void isDoable() throws Exception {
        this.stocks.removeIngredientsFromCookie(cookieSample, 4);

        assertEquals(false, this.stocks.isDoable(cookieSample));

        this.stocks.addToDough(Dough.CHOCO, 1);

        assertEquals(true, this.stocks.isDoable(cookieSample));

        this.stocks.removeIngredientsFromCookie(cookieSample, 1);

        assertEquals(false, this.stocks.isDoable(cookieSample));
    }

}