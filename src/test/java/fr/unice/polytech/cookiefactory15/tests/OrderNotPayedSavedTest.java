package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;
import fr.unice.polytech.cookiefactory15.stateOrder.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(OrderNotPayedSaved.class)
@RunWith(PowerMockRunner.class)

public class OrderNotPayedSavedTest {

    private StateOrder stateOrder;
    private Customer client;
    private Stock stock = new Stock();
    private Map<Orderable, Integer> cookies_to_order;

    @Before
    public void init() {
        this.stateOrder = new OrderNotPayedSaved();
        this.client = new Customer("Marcel");
        client.createAccount();
        this.client.getCustomerAccount().becomePremium();

        stock.addToTopping(Topping.WHITE_CHOCO, 100);
        stock.addToTopping(Topping.MILK_CHOCO, 100);
        stock.addToTopping(Topping.M_AND_M, 100);
        stock.addToTopping(Topping.REESE_BUTTERCUP, 100);
        stock.addToFlavour(Flavour.VANILLA, 100);
        stock.addToFlavour(Flavour.CHILI, 100);
        stock.addToFlavour(Flavour.CINNAMON, 100);
        stock.addToDough(Dough.CHOCO, 100);
        stock.addToDough(Dough.PLAIN, 100);
        stock.addToDough(Dough.OATMEAL, 100);
        stock.addToDough(Dough.PEANUT_BUTTER, 100);

        this.cookies_to_order = new HashMap<>();
        Orderable cookie1 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.TOPPED).build();
        Orderable cookie2 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();

        cookies_to_order.put(cookie1, 50);
        cookies_to_order.put(cookie2, 50);

        for (Orderable o : cookies_to_order.keySet()) {
            stock.removeIngredientsFromCookie(o, cookies_to_order.get(o));
        }

    }

    @Test
    public void checkHourTestOrderNotPayedSaved() throws Exception {

        assertEquals(OrderNotPayedNotSaved.class, stateOrder.checkHour(cookies_to_order, stock).getClass());
        assertEquals(100, stock.getDoughIngredientAmount(Dough.OATMEAL));
    }

    @Test
    public void deleteCookiesTestOrderNotPayedSaved3HoursBefore() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        cookies_to_delete.put(cookie, 20);


        assertEquals(OrderNotPayedSaved.class, stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock).getClass());

        assertEquals(30, cookies_to_order.get(cookie).intValue());
        assertEquals(20, stock.getDoughIngredientAmount(Dough.OATMEAL));

    }

    @Test(expected = Exception.class)
    public void deleteCookiesTestOrderNotPayedSaved30HoursBefore() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 16:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        cookies_to_delete.put(cookie, 20);


        stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock);
    }

    @Test
    public void deleteCookiesTestOrderNotPayedSavedCanceledOrder() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie1 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.TOPPED).build();
        Orderable cookie2 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        cookies_to_delete.put(cookie1, 50);
        cookies_to_delete.put(cookie2, 50);


        assertEquals(OrderCanceled.class, stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock).getClass());
        assertEquals(100, stock.getFlavourIngredientAmount(Flavour.VANILLA));
        assertEquals(0, cookies_to_order.size());
    }

    @Test
    public void cancelOrder3HBeforeDateOrder() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        assertEquals(OrderCanceled.class, stateOrder.cancelOrder(cookies_to_order, dateOrder, client, stock).getClass());
        assertEquals(0, cookies_to_order.size());

    }

    @Test
    public void payTest() throws Exception {
        assertEquals(OrderPayedSaved.class, stateOrder.pay(cookies_to_order, stock).getClass());
    }

    @Test(expected = Exception.class)
    public void validateTest() throws Exception {
        stateOrder.validate();
    }

}
