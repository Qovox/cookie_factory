package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CookieTest {

    private Cookie cookie;

    @Before
    public void setUp() {
        cookie = new Cookie.CookieBuilder().withName("CookieTest")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();
    }

    @Test
    public void testEqualsTrue() {
        Cookie cookieTrue = new Cookie.CookieBuilder().withName("CookieTestFalse")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();

        assertTrue(cookie.equals(cookieTrue));
    }

    @Test
    public void testEqualsFalse() {
        Cookie cookieFalse = new Cookie.CookieBuilder().withName("CookieTestFalse")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED)
                .withTopping(Topping.M_AND_M)
                .build();

        assertFalse(cookie.equals(cookieFalse));
    }

}
