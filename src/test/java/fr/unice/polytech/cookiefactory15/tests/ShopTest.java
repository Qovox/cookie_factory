package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.*;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ShopTest {

    private Shop shop;
    private Manager paul1;
    private Manager marcel;
    private Manager paul2;

    @Before
    public void init() {
        paul1 = new Manager("Paul");
        paul2 = new Manager("Paul");
        marcel = new Manager("Marcel");

        List<Manager> managerList = new ArrayList<>();

        managerList.add(paul1);
        managerList.add(paul2);
        managerList.add(marcel);

        shop = new Shop("O'cookie", "Antibes", State.ARIZONA, LocalTime.of(8, 0), LocalTime.of(18, 0), managerList);

        shop.getStock().addToDough(Dough.CHOCO, 7);
        shop.getStock().addToFlavour(Flavour.VANILLA, 7);
        shop.getStock().addToTopping(Topping.MILK_CHOCO, 7);
    }

    @Test
    public void order() throws Exception {
        Cookie cookieSample = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withTopping(Topping.MILK_CHOCO)
                .withMix(Mix.MIXED)
                .withName("Sooo Choco")
                .build();

        Map<Orderable, Integer> map = new HashMap<>();

        map.put(cookieSample, 3);

        this.shop.order(new Customer("Pierro"), LocalDateTime.of(2017, 12, 12, 10, 0), map, PaymentMode.PAYPAL);

        this.shop.getOrders().get(0).pay();


        assertEquals(1, this.shop.getOrders().size());

        assertTrue(this.shop.orderIsDoable(this.shop.getOrders().get(0)));

        map.put(cookieSample, map.get(cookieSample) + 3);


        Order order = new Order(new Customer("Pierro"), shop, LocalDateTime.of(2017, 12, 12, 10, 0), map, PaymentMode.PAYPAL);

        for (Orderable o : order.getCookie_to_order().keySet()) {
            shop.getStock().removeIngredientsFromCookie(o, order.getCookie_to_order().get(o));
        }
        assertFalse(this.shop.orderIsDoable(order));
    }

    @Test
    public void changeHour() throws Exception {
        assertEquals("08:00", this.shop.openingHour());
        assertEquals("18:00", this.shop.closingHour());

        if (this.shop.getManagerByName(paul1.getName()).isPresent()) {
            this.shop.changeHour(this.shop.getManagerByName(paul1.getName()).get(), "9:00", true);

            assertEquals("09:00", this.shop.openingHour());
        }

        if (this.shop.getManagerByName(paul1.getName()).isPresent()) {
            this.shop.changeHour(this.shop.getManagerByName(paul1.getName()).get(), "19:00", false);

            assertEquals("19:00", this.shop.closingHour());
        }
    }

}