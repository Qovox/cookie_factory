package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.CookieBox;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CookieBoxTest {

    private CookieBox cookieBox;

    @Before
    public void setUp() {
        Cookie cookie1 = new Cookie.CookieBuilder().withName("ChocoCrunchy")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();

        Cookie cookie2 = new Cookie.CookieBuilder().withName("MultiTopping")
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withMix(Mix.TOPPED)
                .withTopping(Topping.WHITE_CHOCO)
                .withTopping(Topping.M_AND_M)
                .withTopping(Topping.REESE_BUTTERCUP)
                .build();

        Cookie cookie3 = new Cookie.CookieBuilder().withName("Peanut")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.PEANUT_BUTTER)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();
        Map<Cookie, Integer> cookiePack = new HashMap<>();
        cookiePack.put(cookie1, 3);
        cookiePack.put(cookie2, 4);
        cookiePack.put(cookie3, 5);
        cookieBox = new CookieBox("Multi Flavour", 16.25, cookiePack);
    }

    @Test
    public void testNbCookie() {
        assertEquals(12, cookieBox.getNumberInOrderable());
    }

    @Test
    public void testPrice() {
        assertEquals(16.25, cookieBox.calculateTotalPrice(), 0.01);
    }

    @Test
    public void testDifferentDoughIngredients() {
        assertEquals(3, cookieBox.getDoughToMap().size());
    }

    @Test
    public void testDifferentFlavourIngredients() {
        assertEquals(2, cookieBox.getFlavourToMap().size());
    }

    @Test
    public void testDifferentToppingIngredients() {
        assertEquals(4, cookieBox.getToppingToMap().size());
    }

}
