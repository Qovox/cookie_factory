package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.customers.CustomerAccount;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    private Customer customer;

    @Before
    public void init() {
        TheCookieFactory.get_instance().reset();

        customer = new Customer("Marcel");

        TheCookieFactory.get_instance().addCustomer(customer);
    }

    @Test
    public void orderLessThan30CookiesBeingaMember() {
        TheCookieFactory.get_instance().createAccount(customer);

        if (TheCookieFactory.get_instance().hasAccount(customer.getID())) {
            CustomerAccount account = TheCookieFactory.get_instance().getAccountByID(customer.getID()).get();

            assertFalse(account.isEligible(20));
            assertFalse(account.isEligible(5));
        }

    }

    @Test
    public void orderMoreThan30CookieBeingaMember() {
        TheCookieFactory.get_instance().createAccount(customer);

        if (TheCookieFactory.get_instance().hasAccount(customer.getID())) {
            CustomerAccount account = TheCookieFactory.get_instance().getAccountByID(customer.getID()).get();

            assertFalse(account.isEligible(35));
            assertTrue(account.isEligible(20));
        }
    }

}
