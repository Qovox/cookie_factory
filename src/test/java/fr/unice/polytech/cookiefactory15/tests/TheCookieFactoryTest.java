package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.Shop;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(TheCookieFactory.class)
@RunWith(PowerMockRunner.class)

public class TheCookieFactoryTest {

    private TheCookieFactory theCookieFactory = TheCookieFactory.get_instance();

    @Before
    public void init() {
        TheCookieFactory.get_instance().reset();
    }

    @Test
    public void getCustomerByNameTest() {
        Customer customer = new Customer("Marcel");
        theCookieFactory.addCustomer(customer);
        assertEquals(customer, theCookieFactory.getCustomerByID(1).get());
        assertEquals(Optional.empty(), theCookieFactory.getCustomerByID(10));
    }

    @Test
    public void getShopByNameTest() {
        Shop shop = new Shop("aShop");
        theCookieFactory.addShop(shop);
        assertEquals(shop, theCookieFactory.getShopByName("aShop").get());
        assertEquals(Optional.empty(), theCookieFactory.getShopByName("Shop2"));
    }

    @Test
    public void getRecipeByNameTest() {
        Cookie cookie = new Cookie.CookieBuilder().withName("RecipeTest").build();
        theCookieFactory.addCookieRecipe(cookie);
        assertEquals(cookie, theCookieFactory.getCookieRecipeByName("RecipeTest").get());
        assertEquals(Optional.empty(), theCookieFactory.getCookieRecipeByName("blabla"));
    }

    @Test
    public void aBasicTestCreateAppointment() {
        LocalDateTime expected = LocalDateTime.parse("2017-12-01 15:00", DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm"));
        LocalDate mock = LocalDate.parse("2017-12-01");
        PowerMockito.mockStatic(LocalDate.class);
        PowerMockito.mockStatic(LocalTime.class);
        when(LocalDate.now()).thenReturn(mock);
        when(LocalTime.now().getHour()).thenReturn(10);
        assertEquals(expected, theCookieFactory.createAppointment(1, 15, 0));
    }

    @Test
    public void createAppointmentWithADelayLessThan2Hours() {
        LocalDateTime expected = LocalDateTime.parse("2017-12-01 12:30", DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm"));
        LocalDate mockDate = LocalDate.parse("2017-12-01");
        PowerMockito.mockStatic(LocalDate.class);
        PowerMockito.mockStatic(LocalTime.class);
        when(LocalDate.now()).thenReturn(mockDate);
        when(LocalTime.now().getHour()).thenReturn(10);
        assertEquals(expected, theCookieFactory.createAppointment(1, 10, 30));
    }

    @Test
    public void isMemberAndHasAccount() {
        Customer customer = new Customer("Marcel");

        assertFalse(theCookieFactory.isCustomer(customer.getID()));

        theCookieFactory.addCustomer(customer);

        assertTrue(theCookieFactory.isCustomer(customer.getID()));
        assertFalse(theCookieFactory.hasAccount(customer.getID()));

        theCookieFactory.createAccount(customer);

        assertTrue(theCookieFactory.hasAccount(customer.getID()));
    }

}
