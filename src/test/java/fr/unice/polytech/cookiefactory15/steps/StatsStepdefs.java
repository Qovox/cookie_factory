package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.PaymentMode;
import fr.unice.polytech.cookiefactory15.Shop;
import fr.unice.polytech.cookiefactory15.State;
import fr.unice.polytech.cookiefactory15.customers.Customer;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StatsStepdefs {

    private Shop shop;

    @Given("^a shop named \"([^\"]*)\"$")
    public void aShopNamed(String arg0) throws Throwable {
        shop = new Shop(arg0, "Test address", State.MICHIGAN,
                LocalTime.of(9, 45), LocalTime.of(19, 30), new ArrayList<>());
    }

    @When("a customer orders at \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" at \"([^\"]*)\"$")
    public void ordersAtAndAt(String arg0, String arg1, String arg2, String arg3, String arg4) throws Throwable {
        String[] args = {arg0, arg1, arg2, arg3};
        Customer customer = new Customer("Customer");
        int hours;
        int minutes;
        for (int i = 0; i < 4; i++) {
            String[] time = args[i].split(":");
            hours = Integer.parseInt(time[0]);
            minutes = Integer.parseInt(time[1]);
            shop.order(customer, LocalDateTime.of(2017, 11, 12, hours, minutes),
                    new HashMap<>(), PaymentMode.PAYPAL);
        }
    }

    @Then("^stats show \"([^\"]*)\" different moments when displayed$")
    public void statsShowDifferentMomentsWhenDisplayed(String arg0) throws Throwable {
        assertEquals(2, shop.getStats().getOrderByHour("12:30"));
        assertEquals(1, shop.getStats().getOrderByHour("18:45"));
        assertEquals(1, shop.getStats().getOrderByHour("10:00"));
    }

}
