Feature: Display stats

  Background:
    Given a shop named "StatsCookieShop"

  Scenario: display stats
    When a customer orders at "12:30", "12:30", "18:45" and "10:00" at "StatsCookieShop"
    Then stats show "3" different moments when displayed