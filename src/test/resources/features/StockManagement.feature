Feature: Stock Management

  Background:
    Given a new Shop named "Shop" opening with a certain stock

  Scenario: stock
    When the shop named "Shop" recieves an order of 10 cookies
    Then the stock can assure the order
    And the shop named "Shop" recieves an order of 20 cookies
    And the stock cannot assure the order