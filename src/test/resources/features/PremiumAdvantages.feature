Feature: Basic order

  Background:
    Given a customer named "Marcel" create an account

  Scenario: order saved
    When "Marcel" become premium
    And "Marcel" orders 10 cookies at "ShopCookies"
    Then the basket of "Marcel" is saved

  Scenario: cancel items
    When "Marcel" become premium
    And "Marcel" orders 30 cookies at "ShopCookies"
    And "Marcel" delete 10 cookies
    Then "Marcel" has 20 cookies in his basket