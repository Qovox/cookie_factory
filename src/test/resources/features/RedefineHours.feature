Feature: Redefine hours

  Background:
    Given a manager named "Marcel" working at "Marcel's"

  Scenario: redefine open hour
    When "Marcel" redefine open hour of "Marcel's" to "7:30"
    Then shop "Marcel's" open hour is "7:30"

  Scenario: redefine close hour
    When "Marcel" redefine close hour of "Marcel's" to "18:30"
    Then shop "Marcel's" close hour is "18:30"