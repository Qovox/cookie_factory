Feature: Manage recipes

  Background:
    Given A shop with a known recipe Dark Temptation

  Scenario: add recipe
    When The marketing team adds the recipe Marcel's Favorite
    Then The recipe Dark Temptation is a known recipe
    And The recipe Marcel's Favorite is a known recipe

  Scenario: remove recipe
    When The marketing team removes the recipe Dark Temptation
    Then The recipe Dark Temptation is not a known recipe
    And The recipe Marcel's Favorite is not a known recipe