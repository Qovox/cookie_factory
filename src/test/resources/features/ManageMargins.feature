Feature: Manage margins

  Background:
    Given the user of the software who wants to change the different margins and a custom cookie

  Scenario: change the margins of ingredients
    When the user sets the margin of the dough "CHOCO" to "0.1" dollar
    Then the margin of the dough "CHOCO" is "0.1" dollar
    And the price of the dough "CHOCO" is "1.21" dollar

  Scenario: change the margins of custom cookies
    When the user sets the margin to "0.08" dollar of a custom cookies whose total price is "3.65" dollars
    Then the margin of custom cookies is "0.08" dollar
    And the price of the cookie is now "3.75" dollars